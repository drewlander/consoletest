﻿using System;
using Terminal.Gui;
using System.Diagnostics.Tracing;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using System.Timers;
using System.Threading.Tasks;

namespace ConsoleTest
{
    class Program
    {
        private static Dictionary<NetworkInterface, ProgressBar> _networkInterfaces;
        private static Dictionary<string, long> _previousSpeed;
        private static List<NetworkItem> _networkItems;
        private static Label _testLabel; 
        static void Main(string[] args)
        {
            _networkItems = new List<NetworkItem>();
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            _previousSpeed = new Dictionary<string, long>();
            Application.Init();
            _networkInterfaces = new Dictionary<NetworkInterface, ProgressBar>();
            var top = Application.Top;

            // Creates the top-level window to show
            var win = new Window("MyApp")
            {
                X = 0,
                Y = 1, // Leave one row for the toplevel menu

                // By using Dim.Fill(), it will automatically resize without manual intervention
                Width = Dim.Fill(),
                Height = Dim.Fill()
            };
            var nic_location = 3;
            var label_location = 6;
            _testLabel = new Label("Hello Text") { X = 1, Y = 1 };
            win.Add(_testLabel);
            foreach (NetworkInterface adapter in adapters)
            {
                var networkItem = new NetworkItem();
                networkItem.NicName = adapter.Name;
                networkItem.PreviousSpeed = 0;
                _networkItems.Add(networkItem);
                if (adapter.Speed==0)
                {
                    continue;
                }
                IPInterfaceProperties properties = adapter.GetIPProperties();
                IPv4InterfaceStatistics stats = adapter.GetIPv4Statistics();
                var label = new Label(adapter.Description) { X = 3, Y = label_location };
                var progress = new ProgressBar(new Rect(10, label_location, 50, 1));
                _networkInterfaces.Add(adapter, progress);
                Console.WriteLine(adapter.Description);
                Console.WriteLine("     Speed .................................: {0}",
                    adapter.Speed);
                Console.WriteLine("     Output queue length....................: {0}",
                    stats.OutputQueueLength);
                _previousSpeed.Add(adapter.Name, stats.BytesReceived);
                //int bytesReceivedSpeed = (int)(stats.BytesReceived - double.Parse(lblBytesReceived.Text)) / 1024;
                //progress.Fraction = 0.9f;
                //progress.Pulse();
                nic_location += 1;
                label_location += 1;
                win.Add(label, progress);
            }

            // Add some controls, 
            win.Add(
                // The ones with my favorite layout system
                    new Label(3, 18, "Close this fine piece of softare"),
                    new Button(3, 19, "Close") { Clicked = () => { Application.RequestStop(); } });

            top.Add(win);
            var timer = new System.Timers.Timer(50);
            timer.Elapsed += UpdateStatsAsync;
            timer.Enabled = true;

            Application.Run();
        }
        private static void changeLabel(Label label)
        {
            label.Text = "you cahnged me!";
        }

        private static int UpdateNetworkStatistics()
        {
            foreach (KeyValuePair<NetworkInterface, ProgressBar> entry in _networkInterfaces)
                {
                    
                    var networkItem = _networkItems.Find(x => x.NicName == entry.Key.Name);
                    IPv4InterfaceStatistics stats = entry.Key.GetIPv4Statistics();
                    if (networkItem == null)
                    {
                        networkItem = new NetworkItem();
                        networkItem.NicName = entry.Key.Name;
                        networkItem.PreviousSpeed = 0;
                        _networkItems.Add(networkItem);
                    }
                    long previousSpeed = networkItem.PreviousSpeed;
                    
                    var totalSpeed = entry.Key.Speed / 1024;
                   
                    int bytesReceivedSpeed = ((int)stats.BytesReceived - (int)previousSpeed ) / 1024;
                    double currentSpeed = (double) bytesReceivedSpeed / (int)totalSpeed;
                    var speed = currentSpeed * 100;

                entry.Value.Fraction = (float)speed;
                //entry.Value.Fraction = 0.9f;
                //entry.Value.Pulse();
               
                if (entry.Key.Name == "en0")
                {
                    _testLabel.Text = " " + speed.ToString() + " " + stats.BytesReceived.ToString() + " " + networkItem.PreviousSpeed.ToString();
                }
                networkItem.PreviousSpeed = stats.BytesReceived;
            }

            return 1;
        }
        private static async void UpdateStatsAsync(object source, ElapsedEventArgs e)
        {

            await Task.Run(() => UpdateNetworkStatistics());
        }
    }

    public class NetworkItem
    {
        public string NicName {get; set;}
        public long PreviousSpeed { get; set; }

    }
}
